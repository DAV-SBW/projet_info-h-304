#ifndef _PLAYERH_H
#define _PLAYERH_H
#include <string>
#include "Player.h"
#include <iostream>
#include <vector>

using namespace std;
using std::string;

class Board;

class PlayerH:public Player{
private:
    int color;
    int score;
    string black_way;
    string white_way;

public:
    PlayerH(int value);
    string askPosition();
    int getScore(); 
    int play(Board &board, vector<string> vect, bool check_file);
    void setScore(int value);
    void writteFile(int value, string pos);
    void setFileWay(string black_w, string white_w);
};

#endif



