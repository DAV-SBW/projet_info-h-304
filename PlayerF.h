#ifndef _PLAYERF_H
#define _PLAYERF_H
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "Player.h"

using namespace std;
using std::endl;
using std::cout;

class Board; 

class PlayerF:public Player{
private:
    int color;
    int score; 
    string white_way;
    string black_way;
    string way_black_file; 
	string way_white_file; 
public:
	int getScore(); 
    PlayerF(int value);
    string askPosition();
    int play(Board &board, vector<string> vect, bool check_file);
    void setScore(int value);
    void setFileWay(string black_way, string white_way );
};

#endif



