#ifndef _PLAYERIA_H
#define _PLAYERIA_H

#include <string>
#include <iostream>
#include "Player.h"
#include <vector>

using namespace std;

using std::string;

class Board; 

class PlayerIA:public Player{

private:
    int color;
    int score;
    int endofgame;
    string black_way; 
    string white_way; 
	string file_way; 
	int alpha; 
	int beta; 
	vector<string> completed_col;
	vector<string> completed_lin;
	vector<string> my_stable;
	vector<string> opponent_stable;
public:
    PlayerIA(int value, int &end_game);
    string askPosition();
    void writteFile(int value, string pos);
    int play(Board &board, vector<string> vect, bool check_file);
    int getScore();
    void setFileWay(string black_w, string white_w ); 
    vector<vector<string>> triList(vector<string> empty_pos);
    vector<string> getEnnemi(Board &board);
    vector<int> minimax(vector<string> iaPawns, int depth, bool flag, Board &board, int end_game_ia, int alpha, int beta);
    void  setScore(int value);
    vector<string> getPawns(Board &board);
    int NumberofElements(vector<string>vect1, vector<string> vect2);
    int Utility(Board &board );
    int getEnnemiColor(int color);
    void setWay(string way);
    int coinUtility(Board &board);
    int cornersUtility(Board &board);
    int mobilityUtility(Board &board);
    vector<vector<string>> organiseUtility(vector<vector<string>> children);
    /*bool testStableAround(string pos, vector<string> stable);
	bool testAround(Board &board, string pos);
	bool testDiag(Board &board, string pos);
	bool testLin(Board &board, string lin);
	int stabilityValue(Board &board); 
	int getNotStable(Board &board, vector<string> vect, int value);
	bool testCol(Board &board, string col);
	bool testStability( Board &board, string pos );
	void updateStablePos(vector<string> unstable, vector<string> stable);
	vector<string> createInitialUnstable(vector<string> pawns, vector<string> stable);
	void calculStability(Board &board,vector<string> pawns, vector<string> stable );*/
};

#endif



