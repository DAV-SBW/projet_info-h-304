#include <iostream>
#include "PlayerH.h"
#include "Player.h"
#include "Board.h"
#include <string>
#include <fstream>
#include <chrono>
#include <thread>

using namespace std;
using std::endl;
using std::cout;

PlayerH::PlayerH(int value){
		color=value; 
		score=2;	    
}

string PlayerH::askPosition(){
	string pos;
	cout << "Veuillez entrer la position de votre pion : " << endl;
	cin >> pos;  // z3 est une valeur abérante qui nous permet de rentrer dans le while
	while (((int(pos[1]) > 56 || (int(pos[0]) > 104)) || ((int(pos[1]) < 48) || (int(pos[0])) < 97))){
		if (int(pos[1])==48 && int(pos[0]==48)){
			cout << "Vous avez passé votre tour " << endl;
			break;
		}
		cout << "La position choisie n'est pas valide..." << endl;
		cout << "Veuillez entrer une nouvelle position pour votre pion : " << endl;
		cin >> pos;
	}
	return pos;
}

int PlayerH::getScore(){
	return score; 
}

void PlayerH::writteFile(int value, string pos){
	/*écritude dans les fichiers*/
	if (value == 79){
		ofstream fichier_blanc( white_way, ios::app);
		fichier_blanc << pos << endl;
		//fichier_blanc.close();
	}
	else{
		ifstream fichier_t(white_way); 
		while (!(fichier_t.is_open())){
			cout << " Fichier blanc non ouvert " <<  endl; 
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		}
		ofstream fichier_noir( black_way, ios::app);
		fichier_noir << pos << endl;
		//fichier_noir.close();
	}
}

void PlayerH::setScore(int value){
	score = score + value;
}

void PlayerH::setFileWay(string black_w, string white_w){
	black_way=black_w; 
	white_way=white_w;	
}


int PlayerH::play(Board &board, vector<string> vect, bool check_file){ 
	int value=0; 
	string pos = askPosition(); 
	if (int(pos[1])!=48 && int(pos[0]!=48)) {
		// Vérifie si le joueur ne passe pas son tour
		vector<string> dir_score = board.validPosition(pos, vect);
		// Le flag indique si la position choisie est libre ou non
		while (dir_score.size()==0){
			cout << "La position choisie n'est pas valide" << endl; 
			// Si position remplie, on redemande une nouvelle position + vérification
			pos = askPosition();
			dir_score = board.validPosition(pos, vect);
		}
		board.setBoard(pos[1],pos[0],color); // Pose du pion
		cout << "La position choisie est : " << pos << endl; 
		if(check_file == true){
			writteFile(color, pos);
		}
		value = board.returnPawn(color, pos, dir_score);
		board.updateVector(color, pos, dir_score);
	}
	else{
		score+=value;
	}
	return value; 
}

