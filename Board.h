#ifndef _BOARD_H
#define _BOARD_H
#include <iostream>
#include <string>
#include <vector>
using namespace std;
using std::endl;
using std::cout;

class Board
{
private:
    int sizeX;
    int sizeY;
    int tableau[9][9]={42};
    vector<string> black_pawn; 
    vector<string> white_pawn;  
    int totalpawns; 
public:
    Board();
    void setBoard(int x, int y, int value);
    void printboard();
    void setBlackPawn(); 
    void setWhitePawn(); 
    int  getBoard(int x, int y);
    void updateVector(int value, string pos, vector<string> dir); 
    vector<string> deleteInVect(vector<string> vect, int lin, int col);
    vector<string> getWhitePawn(); 
    vector<string> getBlackPawn(); 
    vector<string> validPosition(string pos, vector<string> vect);
    vector<string> checkAround(vector<string> hostile_pawns, int value);
    int checkBoard(int value,int x, int y, int Col, int Line);
    int returnPawn(int value, string pos, vector<string> dir);
    vector<string> addToVect(vector<string> vect, int lin, int col);
    void ReverseReturnPawn(int value, string pos, vector<string> dir);
    void ReverseUpdateVector(int value, string pos, vector<string> dir);
    void setTotalPawns();
    int getTotalPawns();
};

#endif
