
#include "Board.h"
#include "Player.h"
using namespace std;
using std::endl;
using std::cout;

Board::Board()
{
	/* Constructeur */
	string lettre; 
	lettre = "abcdefgh"; 
	for (int i=1; i<9; i++){
		tableau[i][0]=48+i;  
		tableau[0][i]=lettre[i-1];
		for (int j=1; j<9; j++){
			tableau[i][j]=46;
		} 
	}
    tableau[5][4]=88;
    tableau[4][4]=79;
    tableau[4][5]=88;
    tableau[5][5]=79;
    black_pawn=addToVect(black_pawn,5,4); 
    black_pawn=addToVect(black_pawn,4,5);
    white_pawn=addToVect(white_pawn,4,4);
    white_pawn=addToVect(white_pawn,5,5);
    totalpawns=4; 
}

void Board::setTotalPawns(){
	totalpawns = black_pawn.size() + white_pawn.size();
}

vector<string> Board::getBlackPawn(){
	return black_pawn; 
}
vector<string> Board::getWhitePawn(){
	return white_pawn; 
}

void Board::setBlackPawn(){
	
}

void Board::setWhitePawn(){
}
int Board :: getTotalPawns(){
	return totalpawns;
	}
	
void Board::setBoard(int x, int y, int value){
    /* Met les pions sur la position donnée */
    tableau[(x-48)][(y-96)]=value;
}

int Board::getBoard(int x, int y){
	int value= tableau[(x-48)][(y-96)]; 
	return value; 
}

vector<string> Board::addToVect(vector<string> vect, int lin, int col){
	char cole, line; 
	cole =(char)(col+96);
	line =(char)(lin+48); 
	string pos;
	pos.push_back(cole); 
	pos.push_back(line); 
	vect.push_back(pos);
	return vect;
}

vector<string> Board::deleteInVect(vector<string> vect, int lin, int col){
	char cole, line; 
	cole =(char)(col+96);
	line =(char)(lin+48);
	string pos;
	pos.push_back(cole); 
	pos.push_back(line);
	for (int i=0; i<vect.size(); i++){
		if(vect[i]==pos){
			vect.erase(vect.begin()+i); 
			break;
		}
	}
	return vect; 
	}
void Board::printboard(){
	/* lit les int dans le tableau et affiche les caractères ASCII correspondant */
	for (int i=0; i<9; i++){ 
		for (int j=0; j<9; j++){
			cout << char (tableau[i][j]) << "  ";
		}
		cout << "" << endl;
	}
}

vector<string> Board::validPosition(string pos, vector<string> vect){
	/* Reçoit la position en input
	 * vérifie si la position est une position du vecteur empty_pos (positions possibles)
	 * Et si position est dedans, il retourne l'opposé de la direction d'un sandwich possible
	 * et le score de ce retournement */ 
    vector<string> res; 
    for(int elem=0; elem<vect.size(); elem++){
		if(vect[elem].substr(0,2) == pos){
			res.push_back(vect[elem].substr(2,5));
		}
	}	
	return res;
}

vector<string> Board::checkAround(vector<string> hostile_pawns, int value){
	/*Retourne une liste de positions vides qu'on pourrait occuper*/
	vector<string> empty_pos={}; 
	int size=hostile_pawns.size();
	for (int k=0;k<size;k++){ 
		int x=(int)(hostile_pawns[k][1])-48;
		int y=(int)(hostile_pawns[k][0])-96;
		for (int i=-1;i<2;i++){ 
			for (int j=-1;j<2;j++){ 
				int newCol=y+i;
				// Col et line sont les tranformées de NewCol et NewLine en char. 
				int newLine=x+j;
				if((tableau[newLine][newCol] == 46)){
					int res;
					res = checkBoard(value,newLine, newCol, -i, -j);
					if(res != 0){
						char col = (char)(newCol+96); 
						char lin = (char)(newLine+48); 
						string position; 
						position.push_back(col); 
						position.push_back(lin);
						// +3 pour manipuler que des positifs
						position.push_back(((-1*i)+'3'));
						position.push_back(((-1*j)+'3'));
						position.push_back(res+48);
						// Sert pour garder le score pour Minimax
						empty_pos.push_back(position);
					}
				}
			}
		}
	} 
	return empty_pos;
}

int Board::checkBoard(int value,int x, int y, int Col, int Line){		
	/*Check si les pions sont en sandwich*/
	bool res = false; 
	int score=0; 
	int count=1;
	int exit=1; // flag pour sortir du while
	while (exit!=0 && int(y+(Col*count)) <= 8 && int(y+(Col*count)) > 0 && int(x+(Line*count) > 0 && int(x+(Line*count)) <= 8)){
		//While pour ne pas sortir des frontières du plateau
		if (tableau[x+(Line*count)][y+(Col*count)] == 46){
			//Case vide, pas de sandwich
			count=0;
			exit=0;
		}
		else if (tableau[x+(Line*count)][y+(Col*count)] != value){ 
			//Il avance dans la direction en inspectant qu'il y ait bien des pions adverses
			count++;  
		}
		else {
			//On atteint un de nos pions => on ferme un sandwich
			score=count-1; 
			//returnPawn(value, x, y, Col, Line, count);  
			exit=0; 
		}
	}
	return score; 
}

void Board::updateVector(int value, string pos, vector<string> dir){
	int x = pos[1]-48; 
	int y = pos[0]-96; 
	if(value==79){
		white_pawn=addToVect(white_pawn, x, y);
		for(int j=0; j<dir.size(); j++){
			int count=dir[j][2]-48;
			int Line=dir[j][1]-'3';
			int Col=dir[j][0]-'3';
			for (int num=1; num<count+1; num++){
				white_pawn=addToVect(white_pawn, x+(Line*num), y+(Col*num));
				black_pawn=deleteInVect(black_pawn, x+(Line*num), y+(Col*num));
			}
		}
	}
	else{
		black_pawn=addToVect(black_pawn, x, y);
		for(int j=0; j<dir.size(); j++){
			int count=dir[j][2]-48;
			int Line=dir[j][1]-'3';
			int Col=dir[j][0]-'3';
			for (int num=1; num<count+1; num++){
				black_pawn=addToVect(black_pawn, x+(Line*num), y+(Col*num));
				white_pawn=deleteInVect(white_pawn, x+(Line*num), y+(Col*num));
			}
		}
	}
}
			

int Board::returnPawn(int value, string pos, vector<string> dir){ 
	/*On retourne les pions capturés
	 *Le vector dir contient : direction et le count 
	 *Où le count représente le nombre de pions retournés dans cette direction
	 * */
	int x = pos[1]-48; 
	int y = pos[0]-96; 
	int score=0;
	for(int j=0; j<dir.size(); j++){
		int count=dir[j][2]-48;
		score+=count; 
		int Line=dir[j][1]-'3';
		int Col=dir[j][0]-'3';
		for (int num=1; num<count+1; num++){
			tableau[x+(Line*num)][y+(Col*num)] = value;
		} 
	}
	return score;
}

void Board::ReverseReturnPawn(int value, string pos, vector<string> dir){
	/*On revient en arrière dans la pose des pions (ce qui a été retourné ne l'est plus) 
	 * C'est une fonction utilisé pour le minmax lorsqu'on remonte vers la racine 
	 * On doit revenir au board initial. 
	 * */
	int x = pos[1]-48; 
	int y = pos[0]-96; 
	int score=0;
	tableau[x][y]=46;
	for(int j=0; j<dir.size(); j++){
		int count=dir[j][2]-48;
		score+=count; 
		int Line=dir[j][1]-'3';
		int Col=dir[j][0]-'3';
		for (int num=1; num<count+1; num++){
			tableau[x+(Line*num)][y+(Col*num)] = value;
		} 
	} 
	
}

 void Board::ReverseUpdateVector(int value, string pos, vector<string> dir){
	/*Annule le coup joué*/
	int x = pos[1]-48; 
	int y = pos[0]-96; 
	if(value==79){
		white_pawn=deleteInVect(white_pawn, x, y);
		for(int j=0; j<dir.size(); j++){
			int count=dir[j][2]-48;
			int Line=dir[j][1]-'3';
			int Col=dir[j][0]-'3';
			for (int num=1; num<count+1; num++){
				white_pawn=deleteInVect(white_pawn, x+(Line*num), y+(Col*num));
				black_pawn=addToVect(black_pawn, x+(Line*num), y+(Col*num));
			}
		}
	}
	else{
		black_pawn=deleteInVect(black_pawn, x, y);
		for(int j=0; j<dir.size(); j++){
			int count=dir[j][2]-48;
			int Line=dir[j][1]-'3';
			int Col=dir[j][0]-'3';
			for (int num=1; num<count+1; num++){
				black_pawn=deleteInVect(black_pawn, x+(Line*num), y+(Col*num));
				white_pawn=addToVect(white_pawn, x+(Line*num), y+(Col*num));
			}
		}
	}
} 
			

