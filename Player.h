#ifndef _PLAYER_H
#define _PLAYER_H
#include <iostream>
#include <string>
#include <vector>

class PlayerH;
class PlayerF;
class PlayerIA;

using namespace std;
using std::string;

class Board;

class Player
{
protected:
	string way_black_file; 
	string way_white_file; 
	string white_way;
	string black_way;
    int color;
    int score;
public: 
    virtual int getScore()=0; 
    virtual string askPosition()=0;
    virtual int play(Board &board, vector<string> vect, bool check_file)=0;
    virtual void setScore(int value);
    virtual void setFileWay(string black_way, string white_way );
};

#endif 
