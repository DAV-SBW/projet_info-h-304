//#include <iostream>
#include "PlayerF.h"
#include "PlayerIA.h"
#include "PlayerH.h"
#include "Board.h"
#include <string>
#include <chrono>
#include <thread>

using namespace std;
using std::endl;
using std::cout;
using std::getline;

class Projet{
private:
	bool check_file;
    int white_score;
    int black_score; 
    Player* blackPlayer= NULL;
    Player* whitePlayer= NULL;
    Board board;
    int end_game=0; 
public:
    Projet(){
       createPlayer( blackPlayer, 88);
       createPlayer( whitePlayer, 79);
       createWay(); 
    }
    int getwhiteScore(){
		return white_score;
    }
    int getblackScore(){
		return black_score;
    }
    Board getBoard(){
		return board;
	}
	int getEndGame(){
		return end_game;
	}

void createPlayer(Player* &player, int color){
	/*Choix du type de joueur*/ 
	cout << "veuillez choisir votre type : H, A ou F " << endl; 
	string type = "Z";
	while ( (type != "H") && (type != "A") && (type != "F")){ 
		cin >> type; 
		if (type=="H"){
			PlayerH* human = new PlayerH(color);
			player= human; 
		}
		else if(type == "A"){
			PlayerIA* artificial = new PlayerIA(color, end_game); 
			player=artificial; 
		}
		else if(type == "F"){
			check_file = true;
			PlayerF* file = new PlayerF(color); 
			player=file;  
		}
		else {
			cout << "veuillez choisir votre type : H, A ou F " << endl; 
		}
	}
}

void createWay(){ // SIMPLIFIER EN DONNANT JUSTE LE CHEMIN QUI LE CONCERNE DONC IL FAUT SUPPRIMER UN DES 2 ATTRIBUTS !!!
	if (check_file == true){
		cout << "Veuillez entrer le chemin du dossier contenant les fichiers noir.txt et blanc.txt" << endl; 
		string way; 
		cin >> way;
		whitePlayer->setFileWay(way + "/noir.txt", way + "/blanc.txt"); 
		blackPlayer->setFileWay(way + "/noir.txt", way + "/blanc.txt"); 
	}
}

void playGame(){
	/*Organise le jeu*/
	int tour=1;
	cout << "Bienvenue pour une nouvelle partie de Reversi ! " << endl;
	int end_game=0; 
	vector <string> possible_pos; 
    while ( (end_game < 2) && ((white_score+ black_score) < 64) ){
		/* end_game représente le moment où il n'y a plus de coup possible après que 
		* les joueurs aient passé 2 fois leur tour de manière forcée.
		*/ 
		if (tour%2 == 0){ 	
			// Si le tour est pair alors c'est le tour du joueur blanc. 
			cout << "C'est au tour du joueur blanc !" << endl; 
			possible_pos=board.checkAround(board.getBlackPawn(), 79);
			if (possible_pos.size() != 0){
				int value=whitePlayer->play(board, possible_pos, check_file);
				whitePlayer->setScore(value+1);
				white_score=whitePlayer->getScore();
				blackPlayer-> setScore(-value);
				black_score=blackPlayer->getScore();
				end_game=0;
			}
			else{
				end_game+=1; 
				cout << " Vous ne pouvez pas jouez, rien ne peut être jouer " << endl; 
			}
			board.setTotalPawns();
		}
		else{
			cout << " C'est au tour du joueur noir ! " << endl; 
			possible_pos=board.checkAround(board.getWhitePawn(), 88);
			if (possible_pos.size() != 0){
				int value=blackPlayer->play(board, possible_pos, check_file);
				blackPlayer->setScore(value+1);
				black_score=blackPlayer->getScore();
				whitePlayer->setScore((-value));
				white_score= whitePlayer->getScore();
				end_game=0;
			}
			else{
				end_game+=1;
				cout << " Vous ne pouvez pas jouez, rien ne peut être jouer " << endl; 
			}
			board.setTotalPawns();	
		}
		board.printboard(); 
		cout << "Score du joueur noir : " << black_score << endl;
		cout << "Score du joueur blanc : " << white_score << endl;	
		tour++;	
	}
}

void endGame(){
	/*Gère la fin de jeu*/
	if (white_score < black_score){
		cout << "Joueur noir a gagné la partie avec " << black_score << " pièces." << endl;
		cout << "Félicitations à joueur noir !" << endl; 
	} 
	else if (white_score > black_score){
		cout << "Joueur blanc a gagné la partie avec " << white_score << " pièces." << endl;
		cout << "Félicitations à joueur blanc !" << endl;	
	} 
	else {
		cout << "Egalité ! Pas de gagnant..." << endl; 
	}
}
};

int main(){
    Projet* game = new Projet(); // Création d'une partie de Reversi
    (game->getBoard()).printboard();
    game->playGame();
    game->endGame();
	delete game; 
	return 0;
}

