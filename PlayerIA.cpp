#include "PlayerIA.h" 
#include <vector>
#include <fstream>
#include <iostream>
#include <chrono>
#include <thread>
#include <bits/stdc++.h>
#include "Board.h" 

using namespace std;
using std::endl;
using std::cout;
using std::ifstream;
using std::ofstream;

#define ENDLESS  999999

PlayerIA::PlayerIA(int value,int &end_game){
	color=value;
	score=2;
}

string PlayerIA :: askPosition(){
	return " ";
}


int PlayerIA::getScore(){
	return score; 
}

void PlayerIA :: setScore(int value){
	score = score + value;
}

void PlayerIA::setWay(string way){
	file_way = way; 
}

void PlayerIA::writteFile(int value, string pos){
	/*Test : écritude dans les fichiers*/
	if (value == 79){
	    ofstream fichier_blanc( white_way, ios::app);
	    fichier_blanc << pos << endl;
	    //fichier_blanc.close();
	}
	else{
	    ifstream fichier_t(white_way); 
		while (!(fichier_t.is_open())){
		cout << " Fichier blanc non ouvert " <<  endl; 
		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
	    }
	ofstream fichier_blanc( white_way, ios::app);
	ofstream fichier_noir( black_way, ios::app);
	fichier_noir << pos << endl;
	//fichier_noir.close();
    }
}

int PlayerIA ::play(Board &board, vector<string> possible_pos, bool check_file){
	int res =0;
	int end_game_ia=0; 
	if (possible_pos.size()!=0){
		vector<int> bestchoice = minimax (getPawns(board),8,true,board, end_game_ia, -ENDLESS, ENDLESS);
		string position = to_string(bestchoice[0]);
		char word = (char)(int(position[0])+48);
		string pos; 
		pos.push_back(word); 
		pos.push_back(position[1]);
		if (check_file==true){
			writteFile(color, pos);
		    }
		vector<string> dir_score = board.validPosition(pos,possible_pos);
		board.setBoard(pos[1],pos[0],color); 
		cout << "La position choisie est : " << pos << endl; 
		int returnvalue = board.returnPawn(color,pos,dir_score);// retourne les pions dans le board et renvoie le nombre de pions retourné	
		board.updateVector(color, pos,dir_score);
		res = returnvalue;  
	} 
	return res;
}
	
vector<int> PlayerIA ::minimax(vector<string> iaPawns, int depth, bool flag, Board &board, int end_game_ia, int alpha, int beta){ 
	vector<int> bestchoice;
	if (depth==0 || end_game_ia==2 || board.getTotalPawns()==64){
		vector<int> res;
		int total = board.getTotalPawns();
		if (end_game_ia==2 || board.getTotalPawns()==64){
			if (iaPawns.size()>(total-iaPawns.size())){
				res={00,200};
			}
			else if (iaPawns.size()==(total-iaPawns.size())){
				res={00,60};
			}
			else {
				res={00,0};
			}
		}
		else{
			if(iaPawns.size() == 0){
				res = {00,0};
			}
			else{
				int utility = Utility(board);
				res={00,utility};
			}
		}	
		bestchoice = res;
		return bestchoice;
	}
	if(flag){
		int maxeval=-ENDLESS;
		// ensemble de position qu'on peut occuper
		vector<string> empty_pos= board.checkAround(getEnnemi(board), color);
		if (empty_pos.size() != 0){
			vector<vector<string>> unorganised_children = triList(empty_pos);
			vector<vector<string>> children = organiseUtility(unorganised_children); 
			//analyse de chaqu'une des positions individuellement
			int child =0; 
			int exit = 1; 
			while ((child<children.size()) && (exit != 0 ) ){
				// CHANGEMENT DANS LE BOARD
				// choix de l'occupation d'une place parmi celle qu'elle pouvait occupée
				// changement du board en jouant le coup à cette place 
				vector<string> dir_score = board.validPosition((children[child][0]).substr(0,2),children[child]); // ensemble de directions à explorer pour retourner une pièce (au moins)
				int returnvalue = board.returnPawn(color, (children[child][0]).substr(0,2),dir_score); // retourne les pieces dans le board et on obtient le nombre de pieces retournée
				board.updateVector(color, ((children[child][0])).substr(0,2),dir_score); // actualise les listes des pawns
				int x = int(children[child][0][1]);
				int y = int(children[child][0][0]);
				board.setBoard(x,y,color);
				// PASSAGE A LA PROFONDEUR-1  AVEC LE NOUVEAU BOARD 
				vector<int> eval = minimax(getPawns(board), depth-1, false, board, end_game_ia, alpha, beta); // appel minimax pour l'etape suivant avec le board modifier
				// MEMORISATION DE LA POSITION ET MAXIMISATION A LA PROFONDEUR T
				string pos = ((children[child][0]).substr(0,2)); // position qui vient d'etre explorer sous forme de str
				int coup = (int(pos[0])-96)*10+(int(pos[1])-48);// transformation sous forme d'int colonne ligne
				maxeval = max(maxeval, eval[1]);
				alpha = max(alpha, eval[1]);
				board.ReverseReturnPawn(getEnnemiColor(color), ((children[child])[0]).substr(0,2),dir_score);
				board.ReverseUpdateVector(color, ((children[child])[0]).substr(0,2),dir_score);
				if (beta >= alpha){ 
					exit = 0; 
				}
				bestchoice = {coup, maxeval};    //vecteur qui contient la valeur du meilleur coup et le coup en question
				//REMISE DU BOARD COMME AVANT LE FOR POUR LE PROCHAIN CHILD
				child ++;
			}
		}
		else{
			end_game_ia +=1; 
			vector<int> eval = minimax(getEnnemi(board),  depth-1, true, board, end_game_ia, alpha, beta);
			if (maxeval <= eval[1]){
					maxeval = eval[1];
					bestchoice = {00, eval[1]};
			}
		}
	}			
	else{
		//CAS DE L'OPPOSANT 
		// il essaie de minimiser le resultat de l'ia 
		int mineval = ENDLESS;
		//ensemble des positions qu'il peut occupé
		vector<string> mypawns = getPawns(board);
		int mycolor = getEnnemiColor(color);
		vector<string> empty_pos=board.checkAround(mypawns,mycolor );
		if (empty_pos.size() != 0){	
			vector<vector<string>> children = triList(empty_pos);
			vector<vector<string>> unorganised_children = triList(empty_pos);
			children = organiseUtility(unorganised_children);
			//analyse de chaqu'une des positions individuellement
			int child=0; 
			int exit_adversary = 1; 
			while (child<children.size() && (exit_adversary !=0) ){
				//CHANGEMENT DANS LE BOARD
				//choix de l'occupation d'une place parmi celle qu'il pouvait occupée
				// changement du board en jouant le coup à cette place 
				vector<string> dir_score = board.validPosition((children[child][0]).substr(0,2),children[child]); // ensemble des directions pour retourner
				int returnvalue = board.returnPawn(getEnnemiColor(color), ((children[child][0])).substr(0,2),dir_score);	// retourne les pions dans le board et renvoie le nombre de pions retourné	
				board.updateVector(getEnnemiColor(color), (children[child][0]).substr(0,2),dir_score);// update les listes des pawns 
				int x = int(children[child][0][1]);
				int y = int(children[child][0][0]);
				board.setBoard(x,y,getEnnemiColor(color));
				// PASSAGE A LA PROFONDEUR-1  AVEC LE NOUVEAU BOARD 
				vector<int> eval = minimax(getEnnemi(board),  depth-1, true, board, end_game_ia, alpha, beta);  
				//MEMORISATION DE LA POSITION ET MINIMISATION
				string pos = ((children[child])[0]).substr(0,2);// position qui vient d'etre occupé par le pion ennemi sous forme de st
				int coup = (int(pos[0])-96)*10+(int(pos[1])-48);// transformation de la position sous forme d'int colonneline
				mineval = min(mineval, eval[1]);
				beta = min(beta, eval[1]);
				board.ReverseReturnPawn(color, (children[child][0]).substr(0,2),dir_score);
				board.ReverseUpdateVector(getEnnemiColor(color), (children[child][0]).substr(0,2),dir_score);
				if (beta <= alpha){ 
					exit_adversary=0; 
				}
				bestchoice = {coup, mineval};
				//REMISE DU BOARD COMME AVANT LE FOR POUR LE PROCHAIN CHILD
				child ++; 
			}
		}
		else{
			end_game_ia +=1; 
			vector<int> eval = minimax(getEnnemi(board),  depth-1, true, board, end_game_ia, alpha, beta);
			if (mineval>=eval[1]){
					mineval = eval[1];
					bestchoice = {00, eval[1]};
			}
		}
	}
	return bestchoice;
}

vector<vector<string>> PlayerIA ::triList(vector<string> empty_pos){
	/*Elimine touts les coups qui se répètent sur base de leur position*/
	vector<vector<string>> children;
	while(empty_pos.size() != 0){
		vector<string> vect;
		for(int i=1; i<empty_pos.size(); i++){
			if((empty_pos[0]).substr(0,2) == (empty_pos[i]).substr(0,2)){ 
				empty_pos.erase(empty_pos.begin()+i);
				vect.push_back(empty_pos[i]);
			}
		}
		vect.push_back(empty_pos[0]);
		empty_pos.erase(empty_pos.begin());
		children.push_back(vect);
	} 
	return children;				
}


vector<vector<string>> PlayerIA ::organiseUtility(vector<vector<string>> children ){
	/* Instaure un tri pour améliorer la complexité du minmax */
	int size = children.size(); 
	vector<int> sorted_list; 
	vector<int> unsorted_list; 
	vector<vector<string>> res; 
	for (int i =0; i< size; i++){
		int sum=0;
		for (int j=0; j < children[i].size(); j++){
			char score = children[i][j][4];
			sum += ((int(score))-48);
		}
	sorted_list.push_back(sum); 
	}
	unsorted_list = sorted_list; 
	sort(sorted_list.begin(), sorted_list.end());
	while(sorted_list.size() != 0){	
		for(int elem = 0; elem<unsorted_list.size(); elem++){
			if (sorted_list[0] == unsorted_list[elem]){
				sorted_list.erase(sorted_list.begin());
				res.push_back(children[elem]);
				unsorted_list[elem] = -1; // Valeur impossible pour un score, utilisée pour sauvegarder les positions dans copy_unsorted-list. 
			}
		}
	}
return res; 
}


vector<string> PlayerIA ::getEnnemi(Board &board){
	vector<string> vect;
	if (color == 79){
		vect=board.getBlackPawn();
	}
	else{
		vect=board.getWhitePawn();
	}
	return vect;
}


int PlayerIA:: getEnnemiColor(int color){
	int res;
	if (color==79){
		res = 88;
	}
	else {
		res = 79;
	}
	return res;
}

int PlayerIA:: NumberofElements(vector<string>vect1, vector<string> vect2){ 
	int res = 0;
	for (int i=0;i<vect1.size();i++){
		for(int j =0; j<vect2.size(); j++){
			if (vect1[i]==vect2[j]){
				res++;
			}
		}
	}
	return res ;
}

int PlayerIA :: Utility(Board &board ){
	int mobility; 
	int coin; 
	int corner; 
	int stable; 
	int res; 
	mobility = mobilityUtility(board); 
	coin = coinUtility(board); 
	corner = cornersUtility(board); 
	//stable = stabilityValue(board); 
	res = (45*corner + 15*mobility + 40*coin)/100; 
	return res;
}


void PlayerIA::setFileWay(string black_w, string white_w ){
	black_way=black_w;
	white_way=white_w;
}

vector<string> PlayerIA ::getPawns(Board &board){
	vector<string> vect;
	if (color == 88){
		vect=board.getBlackPawn();
	}
	else{
		vect=board.getWhitePawn();
	}
	return vect;
}

int PlayerIA::coinUtility(Board &board){
	/*Donne l'utility en se basant sur le nombre de pions que chaque joueur détient.*/
	int min_coins; 
	min_coins = getEnnemi(board).size();
	int max_coins;
	max_coins = getPawns(board).size(); 
	int res = 100*((max_coins-min_coins)/(max_coins+min_coins));
	return res; 
}



int PlayerIA::cornersUtility(Board &board){
	/*Donne l'utility en se basant sur le nombre de positions coins que chaque joueur détient.*/
	int my_corners=0; 
	int opponent_corners=0; 
	vector<string> corners={"a1", "h1", "a8", "h8"};
	for (int i=0; i<corners.size(); i++){
		if(board.getBoard(corners[0][1], corners[0][0]) == color ){
			my_corners++; 
		}
		else if(board.getBoard(corners[0][1], corners[0][0]) == getEnnemiColor(color)){
			opponent_corners++;
		}
	}
	int res = 0; 
	if (my_corners+opponent_corners != 0){
		res = 100*((my_corners-opponent_corners)/(my_corners+opponent_corners));
	}
	return res; 	
}

int PlayerIA::mobilityUtility(Board &board){
	/*Donne l'utilité en se basant sur le nombre de possibilité de déplacement des pions de chaque joueur. */
	int my_mobility;
	int opponent_mobility;
	my_mobility=(board.checkAround(getEnnemi(board), color)).size();
	opponent_mobility=(board.checkAround(getPawns(board), getEnnemiColor(color))).size();
	int res=0; 
	if ((my_mobility+opponent_mobility) != 0){
		res = 100*((my_mobility-opponent_mobility)/(my_mobility+opponent_mobility));
	}
	return res;
}

/*
void PlayerIA::calculStability(Board &board,vector<string> pawns, vector<string> stable ){
	vector<string> unstable_initial = createInitialUnstable(pawns, stable);
	updateStablePos(unstable_initial, stable);
}

vector<string> PlayerIA::createInitialUnstable(vector<string> pawns, vector<string> stable){
	vector<string> unstable; 
	int size_stable = stable.size();
	for(int i=0; i<size_stable; i++){
		int iter; 
		iter = find(stable.begin(), stable.end(), pawns[i]);
		if (iter == stable.end()){
			unstable.push_back(pawns[i]); 
		}
	}
	return unstable; 
}


void PlayerIA::updateStablePos(vector<string> unstable, vector<string> stable){
	bool flag=false; 
	while (flag != true){
		bool test_stab; 
		bool sec_flag=false; 
		for(int i=0; i<unstable.size(); i++){
			test_stab = testStability(unstable[i])
			if (test_stab == true){
				sec_flag == true; 
				unstable.remove(unstable.begin()+i); 
				stable.push_back(unstable[i]);
			}
		}
		if( (sec_flag== false) || (unstable.size() == 0)){
			flag = true; 
		}
	}						
}


bool PlayerIA::testStability( Board &board, string pos ){
	bool flag = testAround(board, pos); 
	bool res = false; 
	bool flag_2 = testStableAround(pos); 
	if((flag_2 == true )|| (flag == true)){
		res = true;
	}
	return res; 
}

bool PlayerIA::testCol(Board &board, string col){
	int iter; 
	bool res = true; 
	iter = find(completed_col.begin(), completed_col.end(), col);
	if(iter == completed_col.end()){
		int i = 0; 
		while(i < 8 && res == true){
			i++; 
			if (board.getBoard(48+i, col) == 46 ){
				res = false; 
			}
		}
		if (res == true){
			completed_col.push_back(col);
		}			
	}
	return res; 	
}

int PlayerIA::getNotStable(Board &board, vector<string> vect, int value){
	vector<string> empty_pos = board.checkAround(vect, value); 
	vector<string> not_stable; 
	int x; 
	int y;
	for (int i=0; i <empty_pos.size(); i++){
		string dir_score = validPosition(empty_pos[i], empty_pos);
		for( intj=0; j<dir_score>.size() ; j++){
			x = int(empty_pos[i][0])+int(dir_score[2]-'3');
			y = int(empty_pos[i][1])+int(dir_score[3]-'3');
			char col=(char)(x);
			char lin=(char)(x);
			string pos; 
			pos.push_back(col);
			pos.push_back(lin);
			not_stable.push_back(pos);
		}
	}
	return (not_stable.size())
}

int PlayerIA::stabilityValue(Board &board){
	int res = 0; 
	calculStability(board,getPawns(board), my_stable );
	calculStability(board,getEnnemi(board), opponent_stable );
	int my_total = getPawns(board).size(); 
	int my_n_s = getNotStable(board, getEnnemi(board),color); 
	int my_s = my_stable.size();
	int res_1 = s*1 - n_s;  
	int opponent_total = getPawns(board).size(); 
	int opponent_n_s = getNotStable(board, getPawns(board), getEnnemiColor(color)); 
	int opponent_s = opponent_stable.size();
	int res_2 = my_s*1 - my_n_s;
	if (res_1 + res_2 != 0){
		res = (res_2 - res_1)/(res_2+res_1);
	}
return res; 

}

bool PlayerIA::testLin(Board &board, string lin){
	int iter; 
	bool res = true; 
	iter = find(completed_lin.begin(), completed_lin.end(), lin);
	if(iter == completed_lin.end()){
		int i = 0; 
		while(i < 8 && res == true){
			i++; 
			if (board.getBoard(lin,96+i) == 46 ){
				res = false; 
			}
		}
		if (res == true){
			completed_lin.push_back(lin);
		}			
	}
	return res; 	
}



bool PlayerIA::testDiag(Board &board, string pos){
	string y = pos[0]; 
	string x = pos[1]; 
	int lin[4]= {1, -1, -1 ,1};
	int col[4]= {1, -1, 1 , -1};
	int count; 
	int i=0;
	int exit = 1; 
	bool res= true; 
	while( i<2 && exit != 0 ){ 
		count = 1; 
		while(exit!=0 && int(y+(col[i]*count)) <= 8 && int(y+(col[i]*count)) > 0 && int(x+(lin[i]*count) > 0 && int(x+(lin[i]*count)) <= 8))
		{
			if(board.getBoard(x+(lin[i]*count),y+(col[i]*count)) == 46){
				exit = 0;
				res = false;  
			}
			count ++; 
		}
		i++;
	}
	while( i<4 && exit != 0 ){ 
		count = 1; 
		while(exit!=0 && int(y+(col[i]*count)) <= 8 && int(y+(col[i]*count)) > 0 && int(x+(lin[i]*count) > 0 && int(x+(lin[i]*count)) <= 8))
		{
			if(board.getBoard(x+(lin[i]*count),y+(col[i]*count)) == 46){
				exit = 0; 
				res = false; 
			}
			count++; 
		}
		i++;
	} 
	return res;	
}

bool PlayerIA::testAround(Board &board, string pos){
	bool res = false; 
	bool test_lin = testLin(board, completed_lin, pos[1]); 
	if(test_lin == true){
		bool test_col = testCol(board, completed_col, pos[0]); 
		if(test_col == true){
			bool test_diag = testDiag(board, pos);
			res = test_diag; 
		}
	}
	return res; 		
}

bool PlayerIA::testStableAround(string pos, vector<string> stable){
	bool res = false; 
	vector<vector<int>> 
	vector<string> possible_stable; 
	vector<string> Lin;
	vector<string> Col; 
	int newCol; 
	int newLin;
	char c; 
	char l; 
	for (int i=-1;i<2;i++){ 
			for (int j=-1;j<2;j++){ 
				int newCol=y+i;
				// Col et line sont les tranformées de NewCol et NewLine en char. 
				int newLine=x+j;
				if(newLine != 0 && newCol != 0){
					char col = (char)(newCol+96); 
					char lin = (char)(newLine+48);
					string pos;
					pos.push_back(col); 
					pos.push_back(lin); 
					possible_stable.push_back(pos);
			}
		}
	}
	// check première diagonale
	vect<int> lin={1, -1};
	vect<int> col={1, -1}; 
	int d2=0; 
	int d1=0; 
	int h=0; 
	int v=0; 
	 
	for(int i=0; i<2; i++){
		newCol = int(pos[0]) + col[i]; 
		newLin = int(pos[1]) + lin[i]; 
		c = (char)(newCol+96); 
		l = (char)(newLine+48);
		string pos;
		pos.push_back(col); 
		pos.push_back(lin); 
		int iter = find(stable.begin(), stable.end(), pos);
		if (iter != stable.end()){
			d1++; 
		}
	}
	vect <int>lin={1, -1};
	vect <int> col={-1, 1};
	// check seconde diagonale
	for(int i=0; i<2; i++){
		newCol = int(pos[0]) + col[i]; 
		newLin = int(pos[1]) + lin[i]; 
		c = (char)(newCol+96); 
		l = (char)(newLine+48);
		string pos;
		pos.push_back(col); 
		pos.push_back(lin); 
		int iter = find(stable.begin(), stable.end(), pos);
		if (iter != stable.end()){
			d2++; 	
		}
	}
	lin={0,0};
	col={1,-1};
	// check horizontal
	for(int i=0; i<2; i++){
		newCol = int(pos[0]) + col[i]; 
		newLin = int(pos[1]) + lin[i]; 
		c = (char)(newCol+96); 
		l = (char)(newLine+48);
		string pos;
		pos.push_back(col); 
		pos.push_back(lin); 
		int iter = find(stable.begin(), stable.end(), pos);
		if (iter != stable.end()){
			h++; 	
		}
	}
	lin={1, -1};
	col={0, 0};
	// Check vertical
	for(int i=0; i<2; i++){
		newCol = int(pos[0]) + col[i]; 
		newLin = int(pos[1]) + lin[i]; 
		c = (char)(newCol+96); 
		l = (char)(newLine+48);
		string pos;
		pos.push_back(col); 
		pos.push_back(lin); 
		int iter = find(stable.begin(), stable.end(), pos);
		if (iter < stable.end()){
			v++; 	
		}
	}
	if((h>0) && (d1>0) && (d2>0) && (v>0) ){
		res = true; 
	}
	return res; 		
}

int findInVect(){
}
*/


