#include "PlayerF.h"
#include "Board.h" 
#include <string>
#include <chrono>
#include <thread>
using std::ifstream;
using std::ofstream;
using std::cout;
using std::endl;
using std::string;
using std::getline;

PlayerF::PlayerF(int value){
	color = value; 
	score =2; 
}

string PlayerF::askPosition(){
	return ""; 
}

void PlayerF::setFileWay(string black_w, string white_w ){
	black_way=black_w; 
	white_way=white_w;	
}

int PlayerF::play(Board &board, vector<string> vect, bool check_file){
	int value=0; 
	if(color == 79){
		string answer; 
		//ofstream fichier_blanc(white_way);
		//ofstream fichier_test(white_way);
		fstream fichier_blanc(white_way);
		while(!(getline(fichier_blanc, answer))){
			fichier_blanc.clear(); 
			//fichier_blanc.sync();
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));
			}
		if (int(answer[1])!=48 && int(answer[0]!=48)) {
		// Vérifie si le joueur ne passe pas son tour
		vector<string> dir_score = board.validPosition(answer, vect);
		// Le flag indique si la position choisie est libre ou non
		while (dir_score.size()==0){
			// Si position remplie, on redemande une nouvelle position + vérification
			while(!(getline(fichier_blanc, answer))){
				fichier_blanc.clear();
				//fichier_blanc.sync();
				std::this_thread::sleep_for(std::chrono::milliseconds(2000));
			}
			dir_score = board.validPosition(answer, vect);
		}
		cout << "Vous avez choisi la position : " << answer << endl; 
		board.setBoard(answer[1],answer[0],color);
		value = board.returnPawn(color, answer, dir_score);
		board.updateVector(color, answer, dir_score);
		}
		else if( int(answer[1])==48 && int(answer[0]==48)){
			cout << "Vous avez passez votre tour! "<< endl;
		}
	}
	else{
		string answer; 
		//ofstream fichier_test(black_way);
		ifstream fichier_noir(black_way);
		while(!(getline(fichier_noir, answer))){
			fichier_noir.clear();
			//fichier_noir.sync();
			std::this_thread::sleep_for(std::chrono::milliseconds(2000));
			}
		if ((int(answer[1]) !=48) && (int(answer[0]) !=48)) {
		// Vérifie si le joueur ne passe pas son tour
		vector<string> dir_score = board.validPosition(answer, vect);
		// Le flag indique si la position choisie est libre ou non
		while (dir_score.size()==0){
			// Si position remplie, on redemande une nouvelle position + vérification
			while(!(getline(fichier_noir, answer))){
				fichier_noir.clear();
				std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			}
			dir_score = board.validPosition(answer, vect);
		}
		cout << "Vous avez choisi la position : " << answer << endl; 
		board.setBoard(answer[1],answer[0],color);
		value = board.returnPawn(color, answer, dir_score);
		board.updateVector(color, answer, dir_score);
		score+=(value+1);	
		}
		else if (int(answer[1])==48 && int(answer[0]==48)){
			cout << "Vous avez passez votre tour! "<< endl;
		}
	}
	return value; 
}

int PlayerF::getScore(){
	return score; 
}

void PlayerF ::setScore(int value){
	score += value;
}



