all: Board.o Player.o PlayerH.o PlayerIA.o PlayerF.o Projet.cpp 
	g++ Board.o Player.o PlayerH.o PlayerIA.o PlayerF.o Projet.cpp -o projet
Board.o: Board.cpp
	g++ -c Board.cpp
Player.o: Player.cpp
	g++ -c Player.cpp 
PlayerH.o: PlayerH.cpp
	g++ -c PlayerH.cpp
PlayerIA.o: PlayerIA.cpp
	g++ -c PlayerIA.cpp
PlayerF.o: PlayerF.cpp
	g++ -c PlayerF.cpp
clean:
	rm *.o
